# Global Config
1 - git config --global user.email "laura@exemplo.com"

2 - git config --global user.name "Laura Nome Exemplo"

# Comandos Basicos do Git
1 - git init

2 - git add .

3 - git reset .

4 - git commit -m "Nome do Commit"

5 - git checkout -- .

6 - git log

7 - git commit --amend

8 - git checkout -b branch-name

9 - git merge branch-test1

10 - git checkout -d branch-name

11 - git push